class ArrayHelpers {
  /**
   * Fonte: https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
   * @param {Array} arrayToShuffle
   */
  static shuffleArray(arrayToShuffle) {
    for (let i = arrayToShuffle.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [arrayToShuffle[i], arrayToShuffle[j]] = [
        arrayToShuffle[j],
        arrayToShuffle[i]
      ];
    }
  }
}

module.exports = ArrayHelpers;
