import React from 'react';
import AlmgSimpleTable from '../material-ui/AlmgSimpleTable';

import { SITUACAO_APTOS_STRING } from '../../consts/situacao-cargos';

export const AlmgCandidatoList = props => {
  const { candidatos } = props;

  if (!candidatos || candidatos.length === 0) {
    return null;
  }

  const columns = [
    {
      id: 0,
      columnName: 'numero',
      description: 'Número'
    },
    {
      id: 1,
      columnName: 'nomeUrna',
      description: 'Nome na urna'
    },
    {
      id: 2,
      columnName: 'partido',
      description: 'Partido'
    },
    {
      id: 3,
      columnName: 'votacao',
      description: 'Total de votos'
    },
    {
      id: 4,
      columnName: 'percentual',
      description: '% válidos'
    }
  ];

  const values = candidatos.map(candidato => {
    return {
      id: candidato.numeroCandidato,
      apto: SITUACAO_APTOS_STRING.indexOf(candidato.situacao) !== -1,
      nomeUrna: candidato.nomeUrna,
      numero: candidato.numeroCandidato,
      partido: candidato.partido.sigla,
      percentual: candidato.votacao.percentualVotosValidosFormatado,
      votacao: candidato.votacao.votosNominaisFormatado
    };
  });

  let countEleitos = 0;

  candidatos.forEach(candidato => {
    if (candidato.votacao.eleito === 'S') {
      countEleitos++;
    }
  });

  const eleitos = candidatos.map(candidato => {
    return {
      eleito: candidato.votacao.eleito === 'S',
      countEleitos
    };
  });

  return (
    <AlmgSimpleTable columns={columns} values={values} eleitos={eleitos} />
  );
};
