/**
 * Instanciando socket.io
 */
const io = require('socket.io')();

/**
 * O chalk é utilizado para utilizar
 * cores no console do terminal
 */
const chalk = require('chalk');

/**
 * Service de dados variáveis
 */
// prettier-ignore
const AlmgEleicoes2018DadosVariaveisService =
  require('./src/services/AlmgEleicoes2018DadosVariaveisService');

/**
 * Service de dados fixos
 */
// prettier-ignore
const AlmgEleicoes2018DadosFixosService =
  require('./src/services/AlmgEleicoes2018DadosFixosService');

/**
 * Apelidando log para facilitar a utilização
 * do chalk
 */
const log = console.log;

/**
 * Vetor para controlar os
 * clientes conectados ao
 * servidor
 */
const connectedClients = [];

/**
 * Controle de setInterval
 */
let intervalConnection;

/**
 * Instanciamos o serviço de arquivos variáveis
 * para obter, a partir dos mesmos, a lista de
 * arquivos fixos correspondente de forma correta
 * e atualizada.
 */
// prettier-ignore
const dadosVariaveisService =
  new AlmgEleicoes2018DadosVariaveisService(__dirname);

/**
 * Variável para o service
 * de dados fixos, que será
 * instanciada posteriormente
 */
let dadosFixosService;

/**
 * Indicativo para integração com
 * Squadra (geração de JSONs para TV ALMG)
 */
const integrarComJSONsSquadra = true;

/**
 * Função principal
 */
function initializeService() {
  /**
   * De posse da lista de arquivos fixos, instanciamos
   * o serviço correspondente para processá-los.
   */
  dadosVariaveisService.getAllArquivosFixos().then(arquivosFixos => {
    /**
     * Transformando o vetor de arquivos
     * fixos no caminho correto
     */
    const arquivosFixosWithPath = arquivosFixos.map(
      arquivo => __dirname + '\\src\\assets\\' + arquivo
    );

    /**
     * Instanciando o service
     * de dados fixos
     */
    // prettier-ignore
    dadosFixosService =
      new AlmgEleicoes2018DadosFixosService(arquivosFixosWithPath);

    /**
     * Realizando o parsing
     * dos dados fixos
     */
    dadosFixosService.parseFiles();

    /**
     * Finalmente realizando o parsing
     * dos dados variáveis
     */
    parseDadosVariaveis();
  });
}

/**
 * Parsing dos dados variáveis
 */
async function parseDadosVariaveis() {
  /**
   * Aguardamos o parsing
   */
  await dadosVariaveisService.parseFiles();

  /**
   * Unimos os dados fixos aos variáveis em
   * apenas uma estrutura, que será enviada
   * ao(s) cliente(s). A partir desta estrutura,
   * tudo será processado.
   */
  dadosVariaveisService.mergeData(dadosFixosService.data);

  /**
   * Informamos o sucesso da operação
   */
  log(chalk.green('Dados consolidados'));
}

/**
 * Controle de conexão dos clientes
 */
io.on('connection', client => {
  /**
   * Variável para controlar
   * a quantidade de emissões
   * de dados por parte do
   * servidor
   */
  let emissoes = 1;

  /**
   * Evento para 'subscribeToVotacao', que
   * será emitido pelo cliente
   */
  client.on('subscribeToVotacao', props => {
    /**
     * Obtendo dados vindos do cliente
     */
    const { clientName, interval } = props;

    /**
     * Verificando se determinado cliente
     * já está conectado ao servidor. Em
     * caso afirmativo, desconectamos e
     * iniciamos uma nova conexão. Isso
     * pode ser ocasionado por um refresh no
     * cliente, por exemplo.
     */
    if (connectedClients.indexOf(clientName) !== -1) {
      log(
        chalk.red(
          "Cliente '" + clientName + "'",
          ' já abriu conexão anteriormente.',
          'Sendo assim, será efetuado o disconnect.'
        )
      );

      /**
       * Limpamos a função de intervalo
       * que era então executada repetidamente
       */
      clearInterval(intervalConnection);
    } else {
      /**
       * Caso seja a primeira conexão,
       * guardamos a referência do cliente
       * no vetor de clientes já conectados
       */
      connectedClients.push(clientName);
    }

    /**
     * Informamos que houve conexão
     */
    log(
      chalk.yellow(
        "Cliente '" +
          clientName +
          "' se inscreveu para obter votação em tempo real..."
      )
    );

    /**
     * Aqui executamos uma função a ser repetida
     * diversas vezes (interval)
     */
    intervalConnection = setInterval(async () => {
      /**
       * É feito novamente um parsing dos dados
       * variáveis, pois pode ter havido alguma
       * atualização da apuração
       */
      await parseDadosVariaveis();

      /**
       * Informando no console a enésima emissão
       * de votação
       */
      log(chalk.yellow('Emitindo votação pela ' + emissoes++ + 'ª vez...'));

      /**
       * Realizando a emissão propriamente dita,
       * com base no valor de interval
       */
      client.emit('votacao', dadosVariaveisService.fullData);
    }, interval);
  });
});

/**
 * Definição da porta
 */
const port = 8200;
io.listen(port);

/**
 * Início de tudo
 */
log(chalk.green('Servidor escutando na porta', port));

/**
 * Invocando função principal
 */
initializeService();
