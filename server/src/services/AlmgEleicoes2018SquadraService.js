const parseString = require('xml2js').parseString;
const fs = require('fs');
const path = require('path');
const numeralService = require('./NumeralService');

const CARGOS = require('../consts/config').CARGOS;
const ESTADOS = require('../consts/config').ESTADOS;
const ELEICAO_2014 = require('../consts/config').CODIGO_ELEICAO_2014_TURNO_1;

const chalk = require('chalk');
const log = console.log;

const util = require('util');

fs.readFilePromise = util.promisify(fs.readFile);
fs.accessPromise = util.promisify(fs.access);
parseStringPromise = util.promisify(parseString);

//const PromiseHelpers = require('../helpers/PromiseHelpers');

class AlmgEleicoes2018DadosSquadraService {
  /**
   * Construtor
   */
  constructor(pathFiles, simularDados = false) {
    // this.pathFiles = pathFiles + '\\src\\assets\\';
    // this.filesVariaveis = [];
    // this.votacaoPresidente = [];
    // this.votacaoGovernador = [];
    // this.votacaoSenador = [];
    // this.votacaoDeputadoFederal = [];
    // this.votacaoDeputadoEstadual = [];
    // this.parsedFiles = [];
    // this.consolidatedData = {};
    // /**
    //  * Controle de simulação
    //  */
    // this.simularDados = simularDados;
  }

  async getAllArquivosFixos() {
    this.processArquivosVariaveisList();
    const promises = [];
    const filesFixos = [];

    this.filesVariaveis.forEach(file => {
      const promise = fs.readFilePromise(file, 'latin1').then(xml => {
        return parseStringPromise(xml);
      });
      promises.push(promise);
    });

    const parsedFiles = await Promise.all(promises);

    parsedFiles.forEach(file => {
      const xmlFixo = file.Resultado.$.nomeArquivoDadosFixos + '.xml';
      filesFixos.push(xmlFixo);
    });

    return filesFixos;
  }

  processArquivosVariaveisList() {
    /**
     * Se já houve o processamento
     * anteriormente, podemos
     * ignorar
     */
    if (!!this.filesVariaveis && this.filesVariaveis.length > 0) {
      return;
    }

    ESTADOS.forEach(estado => {
      for (let x = 0; x < CARGOS.length; x++) {
        const cargo = CARGOS[x];
        const xmlVariavel =
          estado.siglaLowerCase +
          '-' +
          cargo.codigo +
          '-' +
          ELEICAO_2014 +
          '-v.xml';

        /**
         * Ignorando BH, que será processada de
         * outra forma
         */
        if (estado.sigla === 'BH') {
          continue;
        }

        if (cargo.descricao === 'Presidente' && !estado.fetchPresidentes) {
          continue;
        }

        if (cargo.descricao === 'Governador' && !estado.fetchGovernadores) {
          continue;
        }

        if (cargo.descricao === 'Senador' && !estado.fetchSenadores) {
          continue;
        }

        if (
          cargo.descricao === 'Deputado Federal' &&
          !estado.fetchDeputadosFederais
        ) {
          continue;
        }

        if (
          cargo.descricao === 'Deputado Estadual' &&
          !estado.fetchDeputadosEstaduais
        ) {
          continue;
        }

        const filePath = this.pathFiles + xmlVariavel;
        this.filesVariaveis.push(filePath);
      }
    });
  }

  async parseFiles() {
    const promises = [];
    this.processArquivosVariaveisList();

    this.filesVariaveis.forEach(file => {
      /**
       * Log
       */
      log(chalk.blue('Processando arquivo variável', path.basename(file)));

      const promise = fs.readFilePromise(file, 'latin1').then(xml => {
        return parseStringPromise(xml);
      });

      promises.push(promise);
    });

    this.parsedFiles = await Promise.all(promises);
    this.extractAllVotos();
  }

  _formatarVotosAbrangencia(currentAbrangencia) {
    const newAbrangencia = Object.assign({}, currentAbrangencia);

    for (let key in newAbrangencia) {
      if (typeof newAbrangencia[key] !== 'number') {
        continue;
      }
      newAbrangencia[key + 'Formatado'] = numeralService.formatValue(
        newAbrangencia[key]
      );
    }

    return newAbrangencia;
  }

  extractAllVotos() {
    this.votacaoPresidente = [];
    this.votacaoGovernador = [];
    this.votacaoSenador = [];
    this.votacaoDeputadoFederal = [];
    this.votacaoDeputadoEstadual = [];

    for (let i = 0; i < this.parsedFiles.length; i++) {
      /**
       * Alias para o arquivo atual
       */
      const currentFile = this.parsedFiles[i];
      const currentAbrangencia = currentFile.Resultado.Abrangencia[0];

      /**
       * Obtendo UF
       */
      const uf = currentAbrangencia.$.codigoAbrangencia;

      /**
       * Montando objeto de cabeçalho
       */
      const {
        cargoPergunta,
        turno,
        fase,
        matematicamenteDefinido,
        dataGeracao,
        horaGeracao,
        divulgaVotacao
      } = currentFile.Resultado.$;

      const cabecalho = {
        codigoCargo: cargoPergunta,
        dataGeracao,
        divulgaVotacao,
        fase,
        horaGeracao,
        matematicamenteDefinido,
        turno,
        uf
      };

      /**
       * Obtendo código do cargo
       */
      const codigoCargo = cargoPergunta;

      /**
       * Montando objeto de abrangência
       */
      const {
        abstencao,
        abstencaoVT,
        codigoAbrangencia,
        comparecimento,
        comparecimentoVT,
        dataTotalizacao,
        eleitoradoApurado,
        eleitoradoApuradoVT,
        eleitoradoNaoApurado,
        eleitoradoNaoApuradoVT,
        horaTotalizacao,
        secoesNaoTotalizadas,
        secoesNaoTotalizadasVT,
        secoesTotalizadas,
        secoesTotalizadasVT,
        tipoAbrangencia,
        totalizacaoFinal,
        votosEmBranco,
        votosEmBrancoVT,
        votosLegenda,
        votosNominais,
        votosNominaisVT,
        votosNulos,
        votosNulosVT,
        votosPendentes,
        votosPendentesVT,
        votosTotalizados,
        votosTotalizadosVT,
        votosValidos,
        votosValidosVT
      } = currentAbrangencia.$;

      /**
       * Abstenção/comparecimento
       */
      const totalAbstencao = +abstencao; // + +(abstencaoVT || 0);
      const totalComparecimento = +comparecimento; // + +(comparecimentoVT || 0);

      /**
       * Eleitorado apurado/não apurado
       */
      const totalEleitoradoApurado = +eleitoradoApurado; // + +(eleitoradoApuradoVT || 0);
      const totalEleitoradoNaoApurado = +eleitoradoNaoApurado; // + +(eleitoradoNaoApuradoVT || 0);

      /**
       * Totalização de seções
       */
      const totalSecoesTotalizadas = +secoesTotalizadas; // + +(secoesTotalizadasVT || 0);
      const totalSecoesNaoTotalizadas = +secoesNaoTotalizadas; // + +(secoesNaoTotalizadasVT || 0);

      /**
       * Votos
       */
      const totalVotosEmBranco = +votosEmBranco; // + +(votosEmBrancoVT || 0);
      const totalVotosNulos = +votosNulos; // + +(votosNulosVT || 0);
      const totalVotosPendentes = +votosPendentes; // + +(votosPendentesVT || 0);
      const totalVotosValidos = +votosValidos; // + +(votosValidosVT || 0);
      const totalVotosNominais = +votosNominais; // + +(votosNominaisVT || 0);
      const totalVotosTotalizados = +votosTotalizados; // + +(votosTotalizadosVT || 0);

      let abrangencia = {
        codigoAbrangencia,
        dataTotalizacao,
        horaTotalizacao,
        tipoAbrangencia,
        totalAbstencao,
        totalComparecimento,
        totalEleitoradoApurado,
        totalEleitoradoNaoApurado,
        totalizacaoFinal,
        totalSecoesNaoTotalizadas,
        totalSecoesTotalizadas,
        totalVotosEmBranco,
        totalVotosNominais,
        totalVotosNulos,
        totalVotosPendentes,
        totalVotosTotalizados,
        totalVotosValidos,
        votosLegenda: +votosLegenda || 0
      };

      abrangencia = this._formatarVotosAbrangencia(abrangencia);

      /**
       * Calculando % de urnas apuradas
       */
      const percentualUrnasApuradas =
        (+abrangencia.totalSecoesTotalizadas /
          (+abrangencia.totalSecoesTotalizadas +
            +abrangencia.totalSecoesNaoTotalizadas)) *
        100;

      const percentualUrnasApuradasFormatado = numeralService.formatPercentual(
        percentualUrnasApuradas
      );

      abrangencia.percentualUrnasApuradas = percentualUrnasApuradas;
      abrangencia.percentualUrnasApuradasFormatado = percentualUrnasApuradasFormatado;

      /**
       * Obtendo votos de coligação
       */
      const votosColigacaoXml = currentAbrangencia.VotoColigacao;
      const votosColigacaoJson = [];

      /**
       * Obtendo votos de partido
       */
      const votosPartidoXml = currentAbrangencia.VotoPartido;
      const votosPartidoJson = [];

      /**
       * Obtendo votos de candidato
       */
      const votosCandidatoXml = currentAbrangencia.VotoCandidato;
      const votosCandidatoJson = [];

      for (let co = 0; co < votosColigacaoXml.length; co++) {
        const votosColigacaoAtual = votosColigacaoXml[co].$;

        const {
          numeroColigacao,
          totalVotosLegenda,
          totalVotosNominais,
          totalVotosNominaisVT
        } = votosColigacaoAtual;

        votosColigacaoJson.push({
          numeroColigacao,
          votosLegenda: +totalVotosLegenda,
          votosNominais: +totalVotosNominais // + +(totalVotosNominaisVT || 0)
        });
      }

      for (let pa = 0; pa < votosPartidoXml.length; pa++) {
        const votosPartidoAtual = votosPartidoXml[pa].$;

        const {
          numeroPartido,
          totalVotosLegenda,
          totalVotosNominais
        } = votosPartidoAtual;

        votosPartidoJson.push({
          numeroPartido,
          votosLegenda: +totalVotosLegenda,
          votosNominais: +totalVotosNominais
        });
      }

      for (let ca = 0; ca < votosCandidatoXml.length; ca++) {
        const votosCandidatoAtual = votosCandidatoXml[ca].$;

        const {
          classificacao,
          eleito,
          numeroCandidato,
          totalVotos,
          totalVotosVT
        } = votosCandidatoAtual;

        votosCandidatoJson.push({
          classificacao,
          eleito,
          numeroCandidato,
          votosNominais: +totalVotos // + +(totalVotosVT || 0)
        });
      }

      switch (codigoCargo.trim()) {
        case '1':
          this.votacaoPresidente.push({
            uf,
            cabecalho,
            abrangencia,
            votosColigacao: [...votosColigacaoJson],
            votosPartido: [...votosPartidoJson],
            votosCandidato: [...votosCandidatoJson]
          });
          break;

        case '3':
          this.votacaoGovernador.push({
            uf,
            cabecalho,
            abrangencia,
            votosColigacao: [...votosColigacaoJson],
            votosPartido: [...votosPartidoJson],
            votosCandidato: [...votosCandidatoJson]
          });
          break;

        case '5':
          this.votacaoSenador.push({
            uf,
            cabecalho,
            abrangencia,
            votosColigacao: [...votosColigacaoJson],
            votosPartido: [...votosPartidoJson],
            votosCandidato: [...votosCandidatoJson]
          });
          break;

        case '6':
          this.votacaoDeputadoFederal.push({
            uf,
            cabecalho,
            abrangencia,
            votosColigacao: [...votosColigacaoJson],
            votosPartido: [...votosPartidoJson],
            votosCandidato: [...votosCandidatoJson]
          });
          break;

        case '7':
          this.votacaoDeputadoEstadual.push({
            uf,
            cabecalho,
            abrangencia,
            votosColigacao: [...votosColigacaoJson],
            votosPartido: [...votosPartidoJson],
            votosCandidato: [...votosCandidatoJson]
          });
          break;

        default:
          console.log(codigoCargo, ' INVÁLIDO!');
      }
    }
    return;
  }

  _insertVotacaoOn(candidatos, votacoes) {
    /**
     * Para cada candidato...
     */
    candidatos.forEach(candidato => {
      // if (candidato.situacao !== '2') {
      //   console.log(candidato.situacao);
      // }

      /**
       * 1o filtro - votações por UF
       */
      const filterUf = votacoes.filter(
        votacao => votacao.uf === candidato.uf
      )[0];

      const { abrangencia, cabecalho, uf } = filterUf;

      /**
       * 2o filtro - votos por número do candidato
       */
      const filterVotacao = filterUf.votosCandidato.filter(
        votoCandidato =>
          votoCandidato.numeroCandidato === candidato.numeroCandidato
      );

      if (!!filterVotacao && filterVotacao.length > 0) {
        const { classificacao, eleito, votosNominais } = filterVotacao[0];

        /**
         * Calculando o percentual de votos válidos
         * e formatando alguns valores
         */
        const percentualVotosValidos =
          (votosNominais / +filterUf.abrangencia.totalVotosValidos) * 100;

        // prettier-ignore
        const percentualVotosValidosFormatado =
          numeralService.formatPercentual(percentualVotosValidos);

        // prettier-ignore
        const votosNominaisFormatado =
          numeralService.formatValue(votosNominais);

        candidato.votacao = {
          abrangencia,
          cabecalho,
          classificacao,
          eleito,
          percentualVotosValidos,
          percentualVotosValidosFormatado,
          uf,
          votosNominais,
          votosNominaisFormatado
        };
      }
    });
  }

  _removeNonValidCandidates(candidates) {
    return candidates.filter(candidate => candidate.semVotacaoNominal === 'N');
  }

  mergeData(dadosFixos) {
    const mergedData = Object.assign({}, dadosFixos);

    /**
     * Retirando candidatos sem votação nominal
     */
    const presidentes = this._removeNonValidCandidates(mergedData.presidentes);
    const governadores = this._removeNonValidCandidates(
      mergedData.governadores
    );
    const senadores = this._removeNonValidCandidates(mergedData.senadores);
    const deputadosFederais = this._removeNonValidCandidates(
      mergedData.deputadosFederais
    );
    const deputadosEstaduais = this._removeNonValidCandidates(
      mergedData.deputadosEstaduais
    );

    /**
     * Inserindo os votos de cada candidato
     */
    this._insertVotacaoOn(presidentes, this.votacaoPresidente);
    this._insertVotacaoOn(governadores, this.votacaoGovernador);
    this._insertVotacaoOn(senadores, this.votacaoSenador);
    this._insertVotacaoOn(deputadosFederais, this.votacaoDeputadoFederal);
    this._insertVotacaoOn(deputadosEstaduais, this.votacaoDeputadoEstadual);

    const processedData = {
      presidentes,
      governadores,
      senadores,
      deputadosFederais,
      deputadosEstaduais
    };

    this.consolidatedData = processedData;
  }

  get files() {
    return [...this.filesVariaveis];
  }

  get fullData() {
    return Object.assign({}, this.consolidatedData);
  }
}

module.exports = AlmgEleicoes2018DadosSquadraService;
