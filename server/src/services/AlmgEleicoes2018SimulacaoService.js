const numeralService = require('./NumeralService');

const ESTADOS = require('../consts/config').ESTADOS;

const StringHelpers = require('../helpers/StringHelpers');
const NumberHelpers = require('../helpers/NumberHelpers');
const DateHelpers = require('../helpers/DateHelpers');
const NumberDispenser = require('../helpers/NumberDispenser');
const ArrayHelpers = require('../helpers/ArrayHelpers');

/**
 * Configuração de simulações
 */
const configSimulacao = require('../consts/config-simulacao');

class AlmgEleicoes2018SimulacaoService {
  /**
   * Construtor
   */
  constructor() {
    this.currentConfigSimulacao = null;

    this.presidentes = [];
    this.presidentesVotacao = [];
    this.votacaoPresidente = [];
    this.votacaoGovernador = [];
    this.votacaoSenador = [];
    this.votacaoDeputadoFederal = [];
    this.votacaoDeputadoEstadual = [];

    this.quantidadeEtapas = null;
    this.percentualPorEtapa = null;

    this._simularDadosFixos();
    this._inicializarConfiguracoesSimulacao();
    this._inicializarSimulacaoVotacao();
  }

  _inicializarConfiguracoesSimulacao() {
    const {
      maxCargosMajoritarios,
      maxPercentualAbstencao,
      maxPercentualBrancos,
      maxPercentualLegenda,
      maxPercentualNulos,
      minCargosMajoritarios,
      minPercentualAbstencao,
      minPercentualBrancos,
      minPercentualLegenda,
      minPercentualNulos,
      preencherLimiteNomes
    } = configSimulacao.config;

    const percentualAbstencoes = this._randomNumberBetween(
      minPercentualAbstencao,
      maxPercentualAbstencao
    );

    const percentualVotosBrancos = this._randomNumberBetween(
      minPercentualBrancos,
      maxPercentualBrancos
    );

    const percentualVotosNulos = this._randomNumberBetween(
      minPercentualNulos,
      maxPercentualNulos
    );

    const quantidadeCargosMajoritarios = this._randomNumberBetween(
      minCargosMajoritarios,
      maxCargosMajoritarios
    );

    const percentualVotosLegenda = this._randomNumberBetween(
      minPercentualLegenda,
      maxPercentualLegenda
    );

    this.currentConfigSimulacao = {
      percentualAbstencoes,
      percentualVotosBrancos,
      percentualVotosLegenda,
      percentualVotosNulos,
      quantidadeCargosMajoritarios,
      preencherLimiteNomes
    };
  }

  _inicializarSimulacaoVotacao() {
    const emptyVotacao = this._getVotacaoZerada();

    this.presidentes.forEach(presidente => {
      presidente.votacao = Object.assign({}, emptyVotacao);
    });
  }

  _getVotacaoZerada() {
    const totalSecoes = 100;

    return {
      abrangencia: {
        /**
         * Definindo 100 seções (urnas) na simulação
         * de forma fixa
         */
        totalSecoes,

        codigoAbrangencia: '',
        dataTotalizacao: '',
        horaTotalizacao: '',
        percentualUrnasApuradas: 0,
        percentualUrnasApuradasFormatado: '0,00%',
        tipoAbrangencia: '',
        totalAbstencao: 0,
        totalAbstencaoFormatado: '0',
        totalComparecimento: 0,
        totalComparecimentoFormatado: '0',
        totalEleitoradoApurado: 0,
        totalEleitoradoApuradoFormatado: '0',
        totalEleitoradoNaoApurado: 0,
        totalEleitoradoNaoApuradoFormatado: '0',
        totalizacaoFinal: 'N',
        totalSecoesNaoTotalizadas: totalSecoes,
        totalSecoesNaoTotalizadasFormatado: '0',
        totalSecoesTotalizadas: 0,
        totalSecoesTotalizadasFormatado: '0',
        totalVotosEmBranco: 0,
        totalVotosEmBrancoFormatado: '0',
        totalVotosNominais: 0,
        totalVotosNominaisFormatado: '0',
        totalVotosNulos: 0,
        totalVotosNulosFormatado: '0',
        totalVotosPendentes: 0,
        totalVotosPendentesFormatado: '0',
        totalVotosTotalizados: 0,
        totalVotosTotalizadosFormatado: '0',
        totalVotosValidos: 0,
        totalVotosValidosFormatado: '0',
        votosLegenda: 0,
        votosLegendaFormatado: '0'
      },

      cabecalho: {
        codigoCargo: '1',
        dataGeracao: '',
        divulgaVotacao: 'N',
        fase: 'S',
        horaGeracao: '',
        matematicamenteDefinido: 'N',
        turno: '1',
        uf: 'BR'
      },

      classificacao: '0',
      eleito: 'N',
      percentualVotosValidos: 0,
      percentualVotosValidosFormatado: '0,00%',
      uf: '',
      votosNominais: 0,
      votosNominaisFormatado: '0'
    };
  }

  definirSimulacao(quantidadeEtapas = 10) {
    if (100 % quantidadeEtapas !== 0) {
      throw new Error('Escolha uma quantidade de etapas divisível por 100!');
    }

    this.quantidadeEtapas = quantidadeEtapas;
    this.percentualPorEtapa = 100 / quantidadeEtapas;
  }

  simularEtapa() {
    /**
     * Trabalhando com presidentes
     */
    const estadosPresidente = ESTADOS.filter(
      estado => !!estado.fetchPresidentes
    );

    estadosPresidente.forEach(estado => {
      const presidentes = this.presidentes.filter(
        presidente => presidente.uf === estado.sigla
      );

      console.log(presidentes);

      //PAREI AQUI
    });

    /**
     * Aplicando votação a Belo Horizonte
     */
    const presidentesBH = this.presidentes.filter(
      presidente => presidente.uf === 'BH'
    );

    const eleitoradoBH =
      configSimulacao.config.eleitoradoPorEstado *
      configSimulacao.config.percentualBH;

    /**
     * Votos a serem aplicados por vez
     */
    const votosParaAplicar = (eleitoradoBH * this.percentualPorEtapa) / 100;

    /**
     * Tornando a abrangência cumulativa
     */
    const currentAbrangencia = presidentesBH[0].votacao.abrangencia;

    /**
     * Simulando abrangência e cabeçalho,
     * que serão aplicados a todos os
     * candidatos
     */
    const abrangenciaCabecalho = this.simularAbrangenciaCabecalho(
      currentAbrangencia,
      'BH',
      votosParaAplicar,
      this.percentualPorEtapa,
      true,
      eleitoradoBH
    );

    /**
     * Objeto de distribuição
     * de números (votos)
     */
    const distribuicaoVotos = new NumberDispenser(
      abrangenciaCabecalho.abrangencia.fatiaAtualVotosNominais
    );

    /**
     * Montando um vetor de índices
     */
    const shuffledArray = Array.from(
      { length: presidentesBH.length },
      (v, i) => i
    );

    /**
     * Embaralhando os índices para
     * deixar a distribuição ainda
     * mais aleatória
     */
    ArrayHelpers.shuffleArray(shuffledArray);

    /**
     * Distribuindo votos a cada candidato
     */
    for (let i = 0; i < presidentesBH.length; i++) {
      /**
       * Apelidando candidato atual
       */
      const candidato = presidentesBH[shuffledArray[i]];

      /**
       * Verificando se o candidato corrente
       * é o último da lista
       */
      const isLastCandidato = i + 1 === presidentesBH.length;

      /**
       * Se o candidato é o último da lista,
       * este recebe todos os votos pendentes
       * de distribuição
       */
      const fatiaVotos = isLastCandidato
        ? distribuicaoVotos.getTheRest()
        : distribuicaoVotos.getRandomNumbers();

      /**
       * Montando objeto de votação
       */
      const newVotacao = {
        abrangencia: Object.assign({}, abrangenciaCabecalho.abrangencia),
        cabecalho: Object.assign({}, abrangenciaCabecalho.cabecalho),
        classificacao: '0',
        eleito: 'N',
        votosNominais: fatiaVotos,
        votosNominaisFormatado: numeralService.formatValue(fatiaVotos),
        uf: candidato.uf,

        // prettier-ignore
        percentualVotosValidos:
          (fatiaVotos / abrangenciaCabecalho.abrangencia.totalVotosNominais) * 100
      };

      /**
       * Unindo a votação ao candidato atual
       */
      this._mergeVotacao(candidato, newVotacao);
    }

    this._analisarEleitos(presidentesBH);

    //console.log(presidentesBH);
    this.presidentesVotacao = presidentesBH;
  }

  _analisarEleitos(candidatos) {
    /**
     * Ordenando conforme percentual
     * de votos válidos
     */
    candidatos.sort(
      (a, b) =>
        b.votacao.percentualVotosValidos - a.votacao.percentualVotosValidos
    );

    /**
     * Regra de
     */
    if (candidatos[0].votacao.percentualVotosValidos > 50) {
      candidatos[0].votacao.eleito = 'S';
      return;
    }

    candidatos[0].votacao.eleito = 'S';
    candidatos[1].votacao.eleito = 'S';
  }

  _mergeVotacao(candidato, newVotacao) {
    candidato.votacao.abrangencia = Object.assign({}, newVotacao.abrangencia);
    candidato.votacao.cabecalho = Object.assign({}, newVotacao.cabecalho);
    candidato.votacao.classificacao = newVotacao.classificacao;
    candidato.votacao.eleito = newVotacao.eleito;
    candidato.votacao.votosNominais += newVotacao.votosNominais;
    candidato.votacao.uf = newVotacao.uf;

    // prettier-ignore
    candidato.votacao.percentualVotosValidos =
      (candidato.votacao.votosNominais / candidato.votacao.abrangencia.totalVotosValidos) * 100;

    /**
     * Formatação de valores
     */
    // prettier-ignore
    candidato.votacao.votosNominaisFormatado =
      numeralService.formatValue(
        candidato.votacao.votosNominais
      );

    // prettier-ignore
    candidato.votacao.percentualVotosValidosFormatado =
      numeralService.formatPercentual(
        candidato.votacao.percentualVotosValidos
      );

    // prettier-ignore
    candidato.votacao.abrangencia.percentualUrnasApuradasFormatado =
      numeralService.formatPercentual(
        candidato.votacao.abrangencia.percentualUrnasApuradas
      );

    // prettier-ignore
    candidato.votacao.abrangencia.totalAbstencaoFormatado =
      numeralService.formatValue(
        candidato.votacao.abrangencia.totalAbstencao
      );

    // prettier-ignore
    candidato.votacao.abrangencia.totalComparecimentoFormatado =
      numeralService.formatValue(
        candidato.votacao.abrangencia.totalComparecimento
      );

    // prettier-ignore
    candidato.votacao.abrangencia.totalEleitoradoApuradoFormatado =
      numeralService.formatValue(
        candidato.votacao.abrangencia.totalEleitoradoApurado
      );

    // prettier-ignore
    candidato.votacao.abrangencia.totalEleitoradoNaoApuradoFormatado =
      numeralService.formatValue(
        candidato.votacao.abrangencia.totalEleitoradoNaoApurado
      );

    // prettier-ignore
    candidato.votacao.abrangencia.totalSecoesNaoTotalizadasFormatado =
      numeralService.formatValue(
        candidato.votacao.abrangencia.totalSecoesNaoTotalizadas
      );

    // prettier-ignore
    candidato.votacao.abrangencia.totalSecoesTotalizadasFormatado =
      numeralService.formatValue(
        candidato.votacao.abrangencia.totalSecoesTotalizadas
      );

    // prettier-ignore
    candidato.votacao.abrangencia.totalVotosEmBrancoFormatado =
      numeralService.formatValue(
        candidato.votacao.abrangencia.totalVotosEmBranco
      );

    // prettier-ignore
    candidato.votacao.abrangencia.totalVotosNominaisFormatado =
      numeralService.formatValue(
        candidato.votacao.abrangencia.totalVotosNominais
      );

    // prettier-ignore
    candidato.votacao.abrangencia.totalVotosNulosFormatado =
      numeralService.formatValue(
        candidato.votacao.abrangencia.totalVotosNulos
      );

    // prettier-ignore
    candidato.votacao.abrangencia.totalVotosPendentesFormatado =
      numeralService.formatValue(
        candidato.votacao.abrangencia.totalVotosPendentes
      );

    // prettier-ignore
    candidato.votacao.abrangencia.totalVotosTotalizadosFormatado =
      numeralService.formatValue(
        candidato.votacao.abrangencia.totalVotosTotalizados
      );

    // prettier-ignore
    candidato.votacao.abrangencia.totalVotosValidosFormatado =
      numeralService.formatValue(
        candidato.votacao.abrangencia.totalVotosValidos
      );

    // prettier-ignore
    candidato.votacao.abrangencia.votosLegendaFormatado =
      numeralService.formatValue(
        candidato.votacao.abrangencia.votosLegenda
      );
  }

  simularAbrangenciaCabecalho(
    currentAbrangencia,
    uf,
    votosParaAplicar,
    percentualPraAplicar = 10,
    cargoMajoritario = false,
    eleitoradoTotal
  ) {
    const now = new Date();
    const formattedNow = DateHelpers.formatDateTime(now);
    const newAbrangencia = Object.assign({}, currentAbrangencia);
    const fatia = percentualPraAplicar / 100;

    newAbrangencia.codigoAbrangencia = uf;
    newAbrangencia.dataTotalizacao = formattedNow.date;
    newAbrangencia.horaTotalizacao = formattedNow.time;
    newAbrangencia.percentualUrnasApuradas += percentualPraAplicar;
    newAbrangencia.tipoAbrangencia = uf;

    // prettier-ignore
    const fatiaAbstencao =
      (votosParaAplicar * this.currentConfigSimulacao.percentualAbstencoes) / 100;

    // prettier-ignore
    const fatiaBrancos =
      (votosParaAplicar * this.currentConfigSimulacao.percentualVotosBrancos) / 100;

    // prettier-ignore
    const fatiaNulos =
      (votosParaAplicar * this.currentConfigSimulacao.percentualVotosNulos) / 100;

    const fatiaVotosNominais =
      votosParaAplicar - fatiaAbstencao - fatiaBrancos - fatiaNulos;

    newAbrangencia.totalAbstencao += fatiaAbstencao;
    newAbrangencia.totalVotosEmBranco += fatiaBrancos;
    newAbrangencia.totalVotosNulos += fatiaNulos;

    newAbrangencia.fatiaAtualVotosNominais = fatiaVotosNominais;
    newAbrangencia.totalVotosNominais += fatiaVotosNominais;
    newAbrangencia.totalComparecimento += votosParaAplicar - fatiaAbstencao;

    newAbrangencia.totalEleitoradoApurado += votosParaAplicar;
    newAbrangencia.totalizacaoFinal = 'N';

    newAbrangencia.totalSecoesNaoTotalizadas -=
      newAbrangencia.totalSecoes * fatia;

    newAbrangencia.totalSecoesTotalizadas += newAbrangencia.totalSecoes * fatia;

    newAbrangencia.totalVotosTotalizados =
      newAbrangencia.totalVotosNominais +
      newAbrangencia.totalVotosEmBranco +
      newAbrangencia.totalVotosNulos;

    newAbrangencia.totalVotosValidos = newAbrangencia.totalVotosTotalizados;

    newAbrangencia.totalEleitoradoNaoApurado =
      eleitoradoTotal - newAbrangencia.totalEleitoradoApurado;

    newAbrangencia.totalVotosPendentes =
      newAbrangencia.totalEleitoradoNaoApurado -
      newAbrangencia.totalAbstencaototalAbstencao;

    // prettier-ignore
    newAbrangencia.votosLegenda = cargoMajoritario
      ? 0
      : (votosParaAplicar * this.currentConfigSimulacao.percentualVotosLegenda) / 100;

    const cabecalho = {
      codigoCargo: '1',
      dataGeracao: formattedNow.date,
      horaGeracao: formattedNow.time,
      divulgaVotacao: 'N',
      fase: 'S',
      matematicamenteDefinido: 'N',
      turno: '1',
      uf: 'BR'
    };

    return {
      abrangencia: Object.assign({}, newAbrangencia),
      cabecalho: Object.assign({}, cabecalho)
    };
  }

  _simularDadosFixos() {
    this.presidentes = this._simulatePresidentes();
  }

  _simulatePresidentes() {
    /**
     * Vetor fixo de presidentes,
     * que será distribuídos nos
     * estados + BR + BH
     */
    const fixedPresidentes = [];

    /**
     * Objeto que irá armazenar
     * todos os candidatos
     * simulados de todos os
     * estados + BR + BH
     */
    const allPresidentes = [];

    const quantidadePresidentes = this._randomNumberBetween(
      configSimulacao.config.minCargosMajoritarios,
      configSimulacao.config.maxCargosMajoritarios
    );

    for (let i = 0; i < quantidadePresidentes; i++) {
      /**
       * Definindo coligação e partido
       * dinamicamente
       */
      // prettier-ignore
      const indexColigacao =
        this._randomNumberBetween(0, configSimulacao.simulacaoColigacoes.length - 1);

      // prettier-ignore
      const coligacao =
        Object.assign({}, configSimulacao.simulacaoColigacoes[indexColigacao]);

      // prettier-ignore
      const partido =
        Object.assign({}, coligacao.partidos[this._randomNumberBetween(0, 1)]);

      /**
       * Definindo nome e data de nascimento dinamicamente
       */
      const nome = this._getNewNome();
      const dataNascimento = this._getNewDataNascimento();

      fixedPresidentes.push({
        uf: '',
        codigoCargo: '1',
        coligacao,
        partido,
        dataNascimento,
        situacao: '2',
        descricaoSituacao: 'Deferido',
        nome,
        nomeUrna: nome,
        numeroCandidato: i + 1,
        semVotacaoNominal: 'N',
        sexo: 'M',
        subJudice: 'N'
      });
    }

    /**
     * Filtrando regiões que devem
     * monitorar o presidente
     */
    const filterEstados = ESTADOS.filter(
      estado => estado.fetchPresidentes === true
    );

    filterEstados.forEach(estado => {
      fixedPresidentes.forEach(presidente => {
        const presidenteUF = Object.assign({}, presidente);
        presidenteUF.uf = estado.sigla;
        allPresidentes.push(Object.assign({}, presidenteUF));
      });
    });

    return allPresidentes;
  }

  _getNewNome() {
    return StringHelpers.getNewNome();
  }

  _getNewDataNascimento() {
    const newDataNascimento = DateHelpers.getNewDataNascimento();
    return newDataNascimento;
  }

  _randomNumberBetween(min, max) {
    return NumberHelpers.getRandomNumberBetween(min, max);
  }

  get dadosSimulados() {
    return Object.assign({}, { presidentes: this.presidentesVotacao });
  }
}

module.exports = AlmgEleicoes2018SimulacaoService;
