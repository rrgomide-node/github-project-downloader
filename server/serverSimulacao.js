/**
 * Instanciando socket.io
 */
const io = require('socket.io')();

/**
 * O chalk é utilizado para utilizar
 * cores no console do terminal
 */
const chalk = require('chalk');

/**
 * Service de simulações
 */
// prettier-ignore
const AlmgEleicoes2018SimulacaoService =
   require('./src/services/AlmgEleicoes2018SimulacaoService');

/**
 * Apelidando log para facilitar a utilização
 * do chalk
 */
const log = console.log;

/**
 * Vetor para controlar os
 * clientes conectados ao
 * servidor
 */
const connectedClients = [];

/**
 * Controle de setInterval
 */
let intervalConnection;

/**
 * Variável que vai guardar o objeto
 * de service de simulação
 */
let simulacaoService;

/**
 * Definição de quantidade de etapas.
 * É importante que seja um valor divisível
 * por 100
 */
const QUANTIDADE_ETAPAS = 10;

/**
 * Função principal
 */
function iniciarServico() {
  /**
   * Instanciando serviço
   */
  simulacaoService = new AlmgEleicoes2018SimulacaoService();

  /**
   * Definindo simulação com determinada
   * quantidade de etapas
   */
  simulacaoService.definirSimulacao(QUANTIDADE_ETAPAS);
}

/**
 * Controle de conexão dos clientes
 */
io.on('connection', client => {
  /**
   * Variável para controlar
   * a quantidade de emissões
   * de dados por parte do
   * servidor
   */
  let emissoes = 1;

  /**
   * Evento para 'subscribeToSimulacao', que
   * será emitido pelo cliente
   */
  client.on('subscribeToSimulacao', props => {
    /**
     * Obtendo dados vindos do cliente
     */
    const { clientName, interval } = props;

    /**
     * Verificando se determinado cliente
     * já está conectado ao servidor. Em
     * caso afirmativo, desconectamos e
     * iniciamos uma nova conexão. Isso
     * pode ser ocasionado por um refresh no
     * cliente, por exemplo.
     */
    if (connectedClients.indexOf(clientName) !== -1) {
      log(
        chalk.red(
          "Cliente '" + clientName + "'",
          ' já abriu conexão anteriormente.',
          'Sendo assim, será efetuado o disconnect.'
        )
      );

      /**
       * Limpamos a função de intervalo
       * que era então executada repetidamente
       */
      clearInterval(intervalConnection);

      /**
       * Reiniciamos o serviço
       */
      iniciarServico();

      //fim do if
    } else {
      /**
       * Caso seja a primeira conexão,
       * guardamos a referência do cliente
       * no vetor de clientes já conectados
       */
      connectedClients.push(clientName);
    }

    /**
     * Informamos que houve conexão
     */
    log(
      chalk.yellow(
        "Cliente '" +
          clientName +
          "' se inscreveu para obter simulação de votação em tempo real..."
      )
    );

    /**
     * Aqui executamos uma função a ser repetida
     * diversas vezes (interval)
     */
    intervalConnection = setInterval(() => {
      /**
       * Verificando o fim da simulação
       */
      if (emissoes > QUANTIDADE_ETAPAS) {
        clearInterval(intervalConnection);
        log(chalk.green.bold('Fim da simulação!'));
        return;
      }

      /**
       * Efetuando a simulação de dados
       * em etapas
       */
      console.log(simulacaoService.dadosSimulados);
      simulacaoService.simularEtapa();

      /**
       * Informando no console a enésima emissão
       * de votação
       */
      log(
        chalk.yellow(
          'Emitindo simulação de votação pela ' + emissoes++ + 'ª vez...'
        )
      );

      /**
       * Realizando a emissão propriamente dita,
       * com base no valor de interval
       */
      client.emit('simulacao', simulacaoService.dadosSimulados);
    }, interval);
  });
});

/**
 * Definição da porta
 */
const port = 8300;
io.listen(port);

/**
 * Início de tudo
 */
log(chalk.green('Servidor de simulação escutando na porta', port));

/**
 * Teste de simulação
 */
// const totalSimulacoes = 10;
// simulacaoService = new AlmgEleicoes2018SimulacaoService();
// simulacaoService.defineSimulation(totalSimulacoes);

// for (let i = 0; i < totalSimulacoes; i++) {
//   simulacaoService.simulate();
// }

/**
 * Invocando função principal
 */
iniciarServico();
