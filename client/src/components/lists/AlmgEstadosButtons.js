import React from 'react';
import Button from '@material-ui/core/Button';
import { Typography } from '@material-ui/core';

import './almgEstadosButtons.css';

const ESTADOS = [
  { nome: 'Brasil', sigla: 'BR' },
  { nome: 'Belo Horizonte', sigla: 'BH' },
  { nome: 'Acre', sigla: 'AC' },
  { nome: 'Alagoas', sigla: 'AL' },
  { nome: 'Amapá', sigla: 'AP' },
  { nome: 'Amazonas', sigla: 'AM' },
  { nome: 'Bahia', sigla: 'BA' },
  { nome: 'Ceará', sigla: 'CE' },
  { nome: 'Distrito Federal', sigla: 'DF' },
  { nome: 'Espírito Santo', sigla: 'ES' },
  { nome: 'Goiás', sigla: 'GO' },
  { nome: 'Maranhão', sigla: 'MA' },
  { nome: 'Mato Grosso', sigla: 'MT' },
  { nome: 'Mato Grosso do Sul', sigla: 'MS' },
  { nome: 'Minas Gerais', sigla: 'MG' },
  { nome: 'Pará', sigla: 'PA' },
  { nome: 'Paraíba', sigla: 'PB' },
  { nome: 'Paraná', sigla: 'PR' },
  { nome: 'Pernambuco', sigla: 'PE' },
  { nome: 'Piauí', sigla: 'PI' },
  { nome: 'Rio de Janeiro', sigla: 'RJ' },
  { nome: 'Rio Grande do Norte', sigla: 'RN' },
  { nome: 'Rio Grande do Sul', sigla: 'RS' },
  { nome: 'Rondônia', sigla: 'RO' },
  { nome: 'Roraima', sigla: 'RR' },
  { nome: 'Santa Catarina', sigla: 'SC' },
  { nome: 'São Paulo', sigla: 'SP' },
  { nome: 'Sergipe', sigla: 'SE' },
  { nome: 'Tocantins', sigla: 'TO' }
];

export const AlmgEstadosButtons = props => {
  const { incluirBR, incluirBH, estado, onClick } = props;

  const estadosBR = incluirBR
    ? [...ESTADOS]
    : [...ESTADOS].filter(estado => estado.sigla !== 'BR');

  const estadosBH = incluirBH
    ? [...estadosBR]
    : [...estadosBR].filter(estado => estado.sigla !== 'BH');

  const filteredEstados = estadosBH;

  const activeEstado = estado === 'BR' && !incluirBR ? 'MG' : estado;

  return (
    <div>
      <Typography align="center">
        {filteredEstados.map(estado => (
          <Button
            color="primary"
            key={estado.sigla}
            onClick={() => onClick(estado.sigla)}
            className={activeEstado === estado.sigla ? 'activeEstado' : ''}
          >
            <span className="monospace">{estado.sigla}</span>
          </Button>
        ))}
      </Typography>
    </div>
  );
};
