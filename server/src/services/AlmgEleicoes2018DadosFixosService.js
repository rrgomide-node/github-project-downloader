const fs = require('fs');
const path = require('path');
const util = require('util');
const parseString = require('xml2js').parseString;

const chalk = require('chalk');
const log = console.log;

fs.readFilePromise = util.promisify(fs.readFile);
parseStringPromise = util.promisify(parseString);

class AlmgEleicoes2018DadosFixosService {
  constructor(arquivosFixosList) {
    this.arquivosFixosList = arquivosFixosList;
    this.parsedFiles = null;
    this.coligacoes = null;
    this.partidos = null;

    this.presidentes = [];
    this.governadores = [];
    this.senadores = [];
    this.deputadosFederais = [];
    this.deputadosEstaduais = [];
  }

  async parseFiles() {
    const promisesFixos = [];
    this.parsedFiles = null;

    this.arquivosFixosList.forEach(file => {
      /**
       * Log
       */
      log(chalk.grey('Processando arquivo fixo', path.basename(file)));

      const promise = fs.readFilePromise(file, 'latin1').then(xml => {
        return parseStringPromise(xml);
      });
      promisesFixos.push(promise);
    });

    const parsedFilesFixos = await Promise.all(promisesFixos);

    this.parsedFiles = parsedFilesFixos;
    this.extractAllCandidatos();

    // console.log(this.presidentes.length);
    // console.log(this.governadores.length);
    // console.log(this.senadores.length);
    // console.log(this.deputadosFederais.length);
    // console.log(this.deputadosEstaduais.length);
  }

  extractAllCandidatos() {
    this.presidentes = [];
    this.governadores = [];
    this.senadores = [];
    this.deputadosFederais = [];
    this.deputadosEstaduais = [];

    for (let i = 0; i < this.parsedFiles.length; i++) {
      const uf = this.parsedFiles[i].DadosFixos.$.codigoAbrangencia;
      const cargo = this.parsedFiles[i].DadosFixos.Cargo;
      const codigoCargo = cargo[0].$.codigo;

      const coligacoes = cargo[0].Coligacao;
      this.coligacoes = [];
      this.partidos = [];

      for (let co = 0; co < coligacoes.length; co++) {
        const coligacao = coligacoes[co].$;
        const partidos = coligacoes[co].Partido;

        const { numero, nome, tipo, composicao } = coligacao;
        this.coligacoes.push({ numero, nome, tipo, composicao });

        if (!!partidos && partidos.length > 0) {
          for (let p = 0; p < partidos.length; p++) {
            const partido = partidos[p].$;
            const candidatos = partidos[p].Candidato;

            const { numero, sigla, nome, semVotacao } = partido;

            this.partidos.push({
              numero,
              sigla,
              nome,
              semVotacao,
              coligacao
            });

            if (!!candidatos && candidatos.length > 0) {
              for (let ca = 0; ca < candidatos.length; ca++) {
                const candidato = candidatos[ca].$;

                const {
                  numero,
                  nome,
                  nomeUrna,
                  sexo,
                  dataNascimento,
                  situacao,
                  descricaoSituacao,
                  semVotacaoNominal,
                  subJudice
                } = candidato;

                switch (codigoCargo.trim()) {
                  case '1':
                    this.presidentes.push({
                      uf,
                      codigoCargo,
                      numeroCandidato: numero,
                      nome,
                      nomeUrna,
                      sexo,
                      dataNascimento,
                      situacao,
                      descricaoSituacao,
                      semVotacaoNominal,
                      subJudice,
                      coligacao,
                      partido
                    });
                    break;

                  case '3':
                    this.governadores.push({
                      uf,
                      codigoCargo,
                      numeroCandidato: numero,
                      nome,
                      nomeUrna,
                      sexo,
                      dataNascimento,
                      situacao,
                      descricaoSituacao,
                      semVotacaoNominal,
                      subJudice,
                      coligacao,
                      partido
                    });
                    break;

                  case '5':
                    this.senadores.push({
                      uf,
                      codigoCargo,
                      numeroCandidato: numero,
                      nome,
                      nomeUrna,
                      sexo,
                      dataNascimento,
                      situacao,
                      descricaoSituacao,
                      semVotacaoNominal,
                      subJudice,
                      coligacao,
                      partido
                    });
                    break;

                  case '6':
                    this.deputadosFederais.push({
                      uf,
                      codigoCargo,
                      numeroCandidato: numero,
                      nome,
                      nomeUrna,
                      sexo,
                      dataNascimento,
                      situacao,
                      descricaoSituacao,
                      semVotacaoNominal,
                      subJudice,
                      coligacao,
                      partido
                    });
                    break;

                  case '7':
                    this.deputadosEstaduais.push({
                      uf,
                      codigoCargo,
                      numeroCandidato: numero,
                      nome,
                      nomeUrna,
                      sexo,
                      dataNascimento,
                      situacao,
                      descricaoSituacao,
                      semVotacaoNominal,
                      subJudice,
                      coligacao,
                      partido
                    });
                    break;
                  default:
                    console.log(codigoCargo, ' INVÁLIDO!');
                }
              }
            }
          }
        }
      }
    }
  }

  get xmlData() {
    return this.xml;
  }

  get data() {
    return {
      deputadosEstaduais: [...this.deputadosEstaduais],
      deputadosFederais: [...this.deputadosFederais],
      governadores: [...this.governadores],
      presidentes: [...this.presidentes],
      senadores: [...this.senadores]
    };
  }
}

module.exports = AlmgEleicoes2018DadosFixosService;
