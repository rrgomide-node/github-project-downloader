const NOMES = require('../consts/nomes');
const NumberHelpers = require('./NumberHelpers');

class StringHelpers {
  static leftPad(value, count = 2, character = '0') {
    if (typeof value !== 'string') {
      throw new Error('O leftPad só funciona para valores do tipo string!');
    }

    const length = value.length;

    if (length >= count) {
      return value;
    }

    const fill = Array.from({ length: count - length })
      .map(() => character)
      .join('');

    return fill + value;
  }

  static getNewNome() {
    const indexNome = NumberHelpers.getRandomNumberBetween(0, 9999);
    return NOMES[indexNome];
  }
}

module.exports = StringHelpers;
