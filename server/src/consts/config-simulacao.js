/**
 * Gerando 10 partidos
 */
const simulacaoPartidos = Array.from({ length: 10 }).map((item, index) => {
  const i = index + 1;
  return {
    nome: 'Partido ' + i,
    numero: i.toString(),
    semVotacao: 'N',
    sigla: 'P' + i
  };
});

/**
 * Gerando 5 coligações
 */
const simulacaoColigacoes = [];

for (let i = 0, j = 1; i < simulacaoPartidos.length; i += 2, j++) {
  const coligacao = {
    composicao:
      simulacaoPartidos[i].sigla + ' / ' + simulacaoPartidos[i + 1].sigla,
    nome: 'Coligação ' + j,
    numero: j.toString(),
    semVotacao: 'N',
    sigla: 'C' + j,
    partidosSigla: [simulacaoPartidos[i].sigla, simulacaoPartidos[i + 1].sigla],
    partidos: [
      Object.assign({}, simulacaoPartidos[i]),
      Object.assign({}, simulacaoPartidos[i + 1])
    ]
  };

  simulacaoPartidos[i].coligacao = Object.assign({}, coligacao);
  simulacaoPartidos[i + 1].coligacao = Object.assign({}, coligacao);
  simulacaoColigacoes.push(Object.assign({}, coligacao));
}

const config = {
  eleitoradoPorEstado: 10000000,
  percentualBH: 0.6,

  maxCandidatosInaptos: 3,
  maxCargosMajoritarios: 10,
  maxDeputados: 100,
  maxPercentualAbstencao: 10,
  maxPercentualBrancos: 10,
  maxPercentualLegenda: 10,
  maxPercentualNulos: 10,

  minCandidatosInaptos: 1,
  minCargosMajoritarios: 5,
  minDeputados: 50,
  minPercentualAbstencao: 1,
  minPercentualBrancos: 1,
  maxPercentualLegenda: 1,
  minPercentualNulos: 1,

  preencherLimiteNomes: false
};

module.exports = {
  config,
  simulacaoColigacoes,
  simulacaoPartidos
};
