import * as React from 'react';
import { Component, Fragment } from 'react';

import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import PersonAdd from '@material-ui/icons/PersonAdd';

import { GOMIDE_THEME as theme, primaryColor } from './theme/gomideTheme';

import './App.css';

import GomideFileInput from './components/files/GomideFileInput';
import { subscribeToGithubDownloader, parseUsers } from './api/subscribe';
import GomideTitle from './components/titles/GomideTitle';
import GomideToolbar from './components/toolbars/GomideToolbar';
import { Grid } from '@material-ui/core';

const styles = {
  inputStyle: {
    width: '50%',
    fontSize: '0.8em',
    padding: 5,
    marginRight: 10
  },

  clickableIconStyle: {
    cursor: 'pointer',
    fontSize: '1.2em',
    marginRight: 10,
    color: primaryColor
  },

  gridStyle: {
    padding: 20
  },

  paintedGridStyle: {
    backgroundColor: '#bdc3c7'
  },

  boldStyle: {
    fontWeight: 'bold'
  }
};

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loadingData: false,
      suffix: '',
      users: []
    };

    /**
     * Bindings
     */
    this.handleUsers = this.handleUsers.bind(this);
    this.downloadRepos = this.downloadRepos.bind(this);

    // if (this.subscribeToVotacao) {
    //   console.log('subscribing to apuração...');
    //   subscribeToVotacao(this.handleDataReceived);
    // }
  }

  handleUsers(usersFile) {
    parseUsers(usersFile, (error, usersFromServer) => {
      const users = usersFromServer.map(user => {
        return {
          name: user,
          status: 'Pendente',
          repoUrl: 'https://github.com/' + user + '/' + user + this.state.suffix
        };
      });

      this.setState({ users });
    });
  }

  downloadRepos() {
    const { users } = this.state;

    subscribeToGithubDownloader(users, (error, usersDownloadStatus) => {
      //     const users = usersFromServer.map(user => {
      //       return {
      //         name: user,
      // status: 'Pendente'
      //       };
      //     });
      //     this.setState({ users });
    });
  }

  renderInputRepositorio = () => {
    return (
      <input
        style={styles.inputStyle}
        type="text"
        placeholder="Sufixo do repositório..."
        onChange={e => this.setState({ suffix: e.target.value })}
      />
    );
  };

  renderRepoUrl = repoUrl => {
    return (
      <a target="_blank" href={repoUrl}>
        {repoUrl}
      </a>
    );
  };

  renderUsers(users) {
    if (!users || users.length === 0) {
      return null;
    }

    return users.map((user, index) => {
      const isEven = index % 2 === 0;

      let currentStyle = { ...styles.gridStyle };

      if (isEven) {
        currentStyle = {
          ...styles.gridStyle,
          ...styles.paintedGridStyle
        };
      }

      /**
       * Corpo da tabela
       */
      return (
        <Fragment key={user.name}>
          <Grid item xs={2} style={currentStyle}>
            <span>{user.name}</span>
          </Grid>

          <Grid item xs={6} style={currentStyle}>
            <span>{this.renderRepoUrl(user.repoUrl)}</span>
          </Grid>

          <Grid item xs={4} style={currentStyle}>
            <span>{user.status}</span>
          </Grid>
        </Fragment>
      );
    });
  }

  renderCabecalho() {
    return (
      <Fragment>
        <Grid item xs={2} style={{ ...styles.gridStyle, ...styles.boldStyle }}>
          <span>Usuário</span>
        </Grid>

        <Grid item xs={6} style={{ ...styles.gridStyle, ...styles.boldStyle }}>
          <span>URL do repositório</span>
        </Grid>

        <Grid item xs={4} style={{ ...styles.gridStyle, ...styles.boldStyle }}>
          <span>
            Status do <em>download</em>
          </span>
        </Grid>
      </Fragment>
    );
  }

  render() {
    const { users, suffix } = this.state;

    /**
     * Cabeçalho da tabela
     */
    const bodyCabecalho = this.renderCabecalho();
    const bodyUsers = this.renderUsers(users);

    return (
      <MuiThemeProvider theme={theme}>
        <Fragment>
          <CssBaseline />

          <GomideTitle>
            Controle de download de repositórios do Github - Raphael Gomide
          </GomideTitle>

          <GomideToolbar>
            {this.renderInputRepositorio()}

            <GomideFileInput onHandleUpload={this.handleUsers}>
              <PersonAdd style={styles.clickableIconStyle} />
            </GomideFileInput>

            <Button
              variant="contained"
              color="primary"
              component="span"
              onClick={this.downloadRepos}
              disabled={!suffix || !users || users.length === 0}
            >
              Download
            </Button>
          </GomideToolbar>

          {!!bodyUsers && (
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
              style={styles.gridStyle}
            >
              {bodyCabecalho}
              {bodyUsers}
            </Grid>
          )}
        </Fragment>
      </MuiThemeProvider>
    );
  }
}
