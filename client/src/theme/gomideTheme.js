import { createMuiTheme } from '@material-ui/core/styles';

export const primaryColor = '#7f8c8d';
export const secondaryColor = '#f39c12';

export const GOMIDE_THEME = createMuiTheme({
  palette: {
    primary: {
      main: primaryColor
    },

    secondary: {
      main: secondaryColor
    }
  },

  direction: 'ltr',

  typography: {
    // Use the system font instead of the default Roboto font.
    htmlFontSize: 10,
    fontFamily: [
      'Roboto',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(',')
  }
});
