import React from 'react';

import './turnos.css';

export const Turnos = props => {
  const { activeTurno, onClick, tem2oTurno } = props;
  return (
    <ul className="turnos">
      <li onClick={() => onClick(1)} className={activeTurno === 1 && 'active'}>
        1º turno
      </li>
      {'  '}

      {tem2oTurno && (
        <li
          onClick={() => onClick(2)}
          className={activeTurno === 2 ? 'active' : ''}
        >
          2º turno
        </li>
      )}
    </ul>
  );
};
