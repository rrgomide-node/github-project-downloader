const NumberHelpers = require('../src/helpers/NumberHelpers');

test('10.000 number generations from 0 to 500', () => {
  let foundZero = false;
  let found500 = false;

  for (let i = 0; i < 10000; i++) {
    const number = NumberHelpers.getRandomNumberBetween(0, 500);

    expect(number).toBeGreaterThanOrEqual(0);
    expect(number).toBeLessThanOrEqual(500);

    if (number === 0) {
      foundZero = true;
    }

    if (number >= 500) {
      found500 = true;
    }
  }

  expect(foundZero).toBe(true);
  expect(found500).toBe(true);
});

test('10.000 number generations from 1 to 100', () => {
  for (let i = 0; i < 10000; i++) {
    const number = NumberHelpers.getRandomNumberBetween(1, 100);

    expect(number).toBeGreaterThanOrEqual(1);
    expect(number).toBeLessThanOrEqual(100);
  }
});
