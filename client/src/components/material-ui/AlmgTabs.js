import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';

function TabContainer({ children, dir }) {
  return (
    <Typography
      component="div"
      dir={dir}
      style={{
        padding: 8 * 3,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
      }}
    >
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired
};

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper
  },

  titleCargo: {
    fontWeight: 'bold'
  },

  flexRow: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between'
  }
});

const _renderDados = (title, value) => {
  return (
    <Fragment>
      <span style={{ fontWeight: 'bold' }}>{title}:</span> {value}
    </Fragment>
  );
};

const AlmgTabs = props => {
  const {
    classes,
    items,
    onHandleTabChange,
    onHandleTabChangeIndex,
    theme,
    value
  } = props;

  const { cargos, abrangencias } = items;

  return (
    <div>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={onHandleTabChange}
          indicatorColor="primary"
          textColor="primary"
          fullWidth
        >
          {cargos.map(candidato => {
            if (!candidato) {
              return null;
            }
            return <Tab key={candidato.id} label={candidato.titulo} />;
          })}
        </Tabs>
      </AppBar>

      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={onHandleTabChangeIndex}
      >
        {cargos.map((cargo, index) => {
          if (!cargo) {
            <Typography style={{ padding: '20px' }}>
              Nenhum dado retornado pelo servidor.
            </Typography>;
          }

          const abrangencia = abrangencias[index];

          if (!abrangencia) {
            return (
              <Typography style={{ padding: '20px' }}>
                Nenhum dado retornado pelo servidor.
              </Typography>
            );
          }

          return (
            <TabContainer dir={theme.direction} key={cargo.id}>
              <div className={classes.flexRow}>
                <div>
                  <Typography>
                    {_renderDados(
                      'Última totalização',
                      abrangencia.dataTotalizacao +
                        ' - ' +
                        abrangencia.horaTotalizacao
                    )}
                  </Typography>

                  <Typography>
                    {_renderDados(
                      'Abstenção',
                      abrangencia.totalAbstencaoFormatado
                    )}
                  </Typography>

                  <Typography>
                    {_renderDados(
                      'Comparecimento',
                      abrangencia.totalComparecimentoFormatado
                    )}
                  </Typography>
                </div>
              </div>

              <div className={classes.flexRow}>
                <div>
                  <Typography>
                    {_renderDados(
                      'Eleitorado apurado',
                      abrangencia.totalEleitoradoApuradoFormatado
                    )}
                  </Typography>
                  {_renderDados(
                    'Eleitorado não apurado',
                    abrangencia.totalEleitoradoNaoApuradoFormatado
                  )}

                  <Typography>
                    {_renderDados(
                      'Nominais',
                      abrangencia.totalVotosNominaisFormatado
                    )}
                  </Typography>
                </div>
              </div>

              <div className={classes.flexRow}>
                <div>
                  <Typography>
                    {_renderDados('Legenda', abrangencia.votosLegendaFormatado)}
                  </Typography>
                  <Typography>
                    {_renderDados(
                      'Brancos',
                      abrangencia.totalVotosEmBrancoFormatado
                    )}
                  </Typography>

                  <Typography>
                    {_renderDados(
                      'Nulos',
                      abrangencia.totalVotosNulosFormatado
                    )}
                  </Typography>
                </div>
              </div>

              <div className={classes.flexRow}>
                <div>
                  <Typography>
                    {_renderDados(
                      'Totalizados',
                      abrangencia.totalVotosTotalizadosFormatado
                    )}
                  </Typography>
                  <Typography>
                    {_renderDados(
                      'Válidos',
                      abrangencia.totalVotosValidosFormatado
                    )}
                  </Typography>
                  <Typography>
                    {_renderDados(
                      '% de urnas apuradas',
                      abrangencia.percentualUrnasApuradasFormatado
                    )}
                  </Typography>
                </div>
              </div>
            </TabContainer>
          );
        })}
      </SwipeableViews>
    </div>
  );
};

AlmgTabs.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(AlmgTabs);
