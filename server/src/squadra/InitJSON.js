const TURNO1_INTENCAO = 'turno1-intencao';
const TURNO1_BOCA_URNA = 'turno1-bocaUrna';
const TURNO1_APURACAO = 'turno1-apuracao';
const TURNO1_RESULTADO = 'turno1-resultado';

const TURNO2_INTENCAO = 'turno2-intencao';
const TURNO2_BOCA_URNA = 'turno2-bocaUrna';
const TURNO2_APURACAO = 'turno2-apuracao';
const TURNO2_RESULTADO = 'turno2-resultado';

const TURNOS_CONSOLIDADO = 'turnos-consolidado';

class InitJSON {
  constructor() {
    this.initJSON = {
      moment: 'turno1-intencao',
      aside: {
        region: [],
        items: [],
        source: []
      },
      page: {
        title: '',
        subTitle: '',
        template: 'home',
        aside: []
      }
    };
  }
}

const initJson = (module.exports = {
  InitJSON
});
