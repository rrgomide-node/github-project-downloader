const NumberHelpers = require('./NumberHelpers');
const StringHelpers = require('./StringHelpers');

const DAYS = Array.from({ length: 28 }).map((item, i) =>
  StringHelpers.leftPad((i + 1).toString(), 2, '0')
);

const MONTHS = Array.from({ length: 12 }).map((item, i) =>
  StringHelpers.leftPad((i + 1).toString(), 2, '0')
);

const YEARS = Array.from({ length: 49 }).map((item, i) =>
  (1940 + i).toString()
);

class DateHelpers {
  /**
   * Obtém data de nascimento aleatória
   * no formato do TSE
   */
  static getNewDataNascimento() {
    const indexYear = NumberHelpers.getRandomNumberBetween(0, 48);
    const indexMonth = NumberHelpers.getRandomNumberBetween(0, 11);
    const indexDay = NumberHelpers.getRandomNumberBetween(0, 27);

    const newDataNascimento =
      DAYS[indexDay] + '/' + MONTHS[indexMonth] + '/' + YEARS[indexYear];

    return newDataNascimento;
  }

  /**
   * Formata a data no padrão do TSE
   * "DD/MM/YYYY"
   * @param {Date} dateTimeToFormat Data/hora a ser formatada
   */
  static formatDateTime(dateTimeToFormat) {
    const year = dateTimeToFormat.getFullYear();
    const month = dateTimeToFormat.getMonth();
    const day = dateTimeToFormat.getDate();
    const hour = dateTimeToFormat.getHours();
    const minutes = dateTimeToFormat.getMinutes();
    const seconds = dateTimeToFormat.getSeconds();

    const date =
      StringHelpers.leftPad(day.toString()) +
      '/' +
      StringHelpers.leftPad((month + 1).toString()) +
      '/' +
      year.toString();

    const time =
      StringHelpers.leftPad(hour.toString()) +
      ':' +
      StringHelpers.leftPad(minutes.toString()) +
      ':' +
      StringHelpers.leftPad(seconds.toString());

    return {
      date,
      time
    };
  }
}

module.exports = DateHelpers;
