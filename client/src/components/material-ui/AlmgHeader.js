/**
 * Fonte: https://github.com/mui-org/material-ui/blob/master/docs/src/pages/demos/app-bar/ButtonAppBar.js
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const styles = {
  root: {
    flexGrow: 1
  },
  flex: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
};

const AlmgHeader = props => {
  const { classes, turno, timeout, showTimeout, countCargas } = props;
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="title" color="inherit" className={classes.flex}>
            ALMG - Eleições 2018 - {turno}º turno
          </Typography>
          <Typography
            variant="title"
            color="inherit"
            align="right"
            className={classes.flex}
          >
            {showTimeout
              ? 'Nova carga em ' + timeout + '...'
              : countCargas + ' carga(s) já efetuada(s)'}
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
};

AlmgHeader.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlmgHeader);
