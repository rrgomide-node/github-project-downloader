/**
 * Fonte: https://github.com/mui-org/material-ui/blob/master/docs/src/pages/demos/progress/CircularIndeterminate.js
 */

import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  progress: {
    margin: theme.spacing.unit * 2
  }
});

const AlmgCircularProgress = props => {
  const { classes, text } = props;
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      {!!text && (
        <Typography variant="title" color="inherit">
          {text}
        </Typography>
      )}
      <CircularProgress className={classes.progress} color="primary" />
    </div>
  );
};

AlmgCircularProgress.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlmgCircularProgress);
