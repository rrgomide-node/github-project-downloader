import React from 'react';

import './cargos.css';

export const Cargos = props => {
  const { cargos, onClick, activeCargo } = props;
  return (
    <ul className="cargos">
      {cargos.map((cargo, index) => (
        <li
          key={cargo}
          onClick={() => onClick(index)}
          className={activeCargo === index ? 'active' : ''}
        >
          {cargo}
        </li>
      ))}
    </ul>
  );
};
