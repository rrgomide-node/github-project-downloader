import React from 'react';

import './gomideToolbar.css';

const GomideToolbar = props => {
  return <div className="gomide-toolbar">{props.children}</div>;
};

export default GomideToolbar;
