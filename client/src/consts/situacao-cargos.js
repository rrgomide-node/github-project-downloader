const SITUACAO_APTOS_NUMBER = [2, 4, 12, 16, 17, 18, 19];
const SITUACAO_CADASTRADOS_NUMBER = [1, 8];
const SITUACAO_INAPTOS_NUMBER = [3, 5, 6, 7, 10, 13, 14];

// prettier-ignore
const SITUACAO_APTOS_STRING = SITUACAO_APTOS_NUMBER.map(number => number.toString());

// prettier-ignore
const SITUACAO_CADASTRADOS_STRING = SITUACAO_CADASTRADOS_NUMBER.map(number => number.toString());

// prettier-ignore
const SITUACAO_INAPTOS_STRING = SITUACAO_INAPTOS_NUMBER.map(number => number.toString());

module.exports = {
  SITUACAO_APTOS_STRING,
  SITUACAO_CADASTRADOS_STRING,
  SITUACAO_INAPTOS_STRING
};
