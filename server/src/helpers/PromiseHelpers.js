class PromiseHelpers {
  /**
   * Fonte: https://stackoverflow.com/questions/17699599/node-js-check-exist-file/32682683
   * @param {Promise} promise
   */
  static toResultObject(promise) {
    return promise
      .then(result => ({ success: true, result }))
      .catch(error => ({ success: false, error }));
  }
}

module.exports = PromiseHelpers;
