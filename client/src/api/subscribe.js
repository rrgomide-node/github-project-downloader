import * as io from 'socket.io-client';

const socketGithubDownloader = io('http://localhost:8400');
const clientName = 'ReactApp';

export function subscribeToGithubDownloader(users, callback) {
  socketGithubDownloader.on('oneDownloadCompleted', users => {
    console.log('Download efetuado');
    console.log(users);
    callback(null, users);
  });

  socketGithubDownloader.emit('downloadGitRepositoriesFromUsers', {
    clientName,
    users
  });
}

export function parseUsers(usersFile, callback) {
  socketGithubDownloader.on('usersParsed', users => {
    console.log('Chegou a lista de usuários');
    console.log(users);
    callback(null, users.users);
  });

  socketGithubDownloader.emit('parseUsers', { clientName, usersFile });
}

// export function disconnect() {
//   console.log('Disconnecting...');
//   if (socketApuracao.connected) {
//     socketApuracao.close();
//   }

//   if (socketSimulacao.connected) {
//     socketSimulacao.close();
//   }
// }
