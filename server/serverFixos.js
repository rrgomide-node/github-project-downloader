const io = require('socket.io')();
const chalk = require('chalk');

const AlmgEleicoes2018DadosFixosService = require('./src/services/AlmgEleicoes2018DadosFixosService.js');
const AlmgEleicoes2018DadosVariaveisService = require('./src/services/AlmgEleicoes2018DadosVariaveisService');

const log = console.log;
let dadosFixosService;

function initializeService() {
  /**
   * Instanciamos o serviço de arquivos variáveis
   * para obter, a partir dos mesmos, a lista de
   * arquivos fixos correspondente de forma correta
   * e atualizada.
   */
  // prettier-ignore
  const dadosVariaveisService =
    new AlmgEleicoes2018DadosVariaveisService(__dirname);

  /**
   * De posse da lista de arquivos fixos, instanciamos
   * o serviço correspondente para processá-los.
   */
  dadosVariaveisService.getAllArquivosFixos().then(arquivosFixos => {
    /**
     * Transformando o vetor de arquivos
     * fixos no caminho correto
     */
    const arquivosFixosWithPath = arquivosFixos.map(
      arquivo => __dirname + '\\src\\assets\\' + arquivo
    );

    // prettier-ignore
    dadosFixosService =
      new AlmgEleicoes2018DadosFixosService(arquivosFixosWithPath);

    dadosFixosService.parseFiles();
  });
}

io.on('connection', client => {
  client.on('subscribeToArquivosFixos', () => {
    log(chalk.green('Cliente se inscreveu para obter lista de candidatos...'));
    log(chalk.yellow('Emitindo lista de candidatos...'));
    client.emit('candidatos', dadosFixosService.data);
  });

  // client.on('subscribeToVotacao', interval => {
  //   log(
  //     chalk.red(
  //       'Cliente se inscreveu para obter votação a cada',
  //       interval / 1000,
  //       'segundos...'
  //     )
  //   );

  //   setInterval(() => {
  //     log(chalk.yellow(emissoes++, ' - Emitindo votação...'));
  //     votacao += 100;
  //     client.emit('votacao', votacao);
  //   }, interval);
  // });
});

const port = 8100;
io.listen(port);
log(chalk.green('Servidor escutando na porta', port));

initializeService();
