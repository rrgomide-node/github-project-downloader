const DateHelpers = require('../src/helpers/DateHelpers');

test('1º de janeiro de 2018', () => {
  const date = new Date(2018, 0, 1);
  const formattedDate = DateHelpers.formatDateTime(date);

  expect(formattedDate.date).toBe('01/01/2018');
  expect(formattedDate.time).toBe('00:00:00');
});

test('28 de fevereiro de 2018', () => {
  const date = new Date(2018, 1, 28, 1, 1, 1);
  const formattedDate = DateHelpers.formatDateTime(date);

  expect(formattedDate.date).toBe('28/02/2018');
  expect(formattedDate.time).toBe('01:01:01');
});

test('22 de dezembro de 1982', () => {
  const date = new Date(1982, 11, 22, 7, 30, 55);
  const formattedDate = DateHelpers.formatDateTime(date);

  expect(formattedDate.date).toBe('22/12/1982');
  expect(formattedDate.time).toBe('07:30:55');
});
