module.exports = {
  CODIGO_ELEICAO_2014_TURNO_1: 'e001431',

  CARGOS: [
    {
      codigo: '0001',
      codigoNumerico: 1,
      descricao: 'Presidente'
    },
    {
      codigo: '0003',
      codigoNumerico: 3,
      descricao: 'Governador'
    },
    {
      codigo: '0005',
      codigoNumerico: 5,
      descricao: 'Senador'
    },
    {
      codigo: '0006',
      codigoNumerico: 6,
      descricao: 'Deputado Federal'
    },
    {
      codigo: '0007',
      codigoNumerico: 7,
      descricao: 'Deputado Estadual'
    }
  ],

  ESTADOS: [
    {
      nome: 'Brasil',
      sigla: 'BR',
      siglaLowerCase: 'br',
      fetchPresidentes: true,
      fetchGovernadores: false,
      fetchSenadores: false,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Minas Gerais',
      sigla: 'MG',
      siglaLowerCase: 'mg',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: true,
      fetchDeputadosEstaduais: true
    },
    {
      nome: 'Belo Horizonte',
      sigla: 'BH',
      siglaLowerCase: 'bh',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: false,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Acre',
      sigla: 'AC',
      siglaLowerCase: 'ac',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Alagoas',
      sigla: 'AL',
      siglaLowerCase: 'al',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Amapá',
      sigla: 'AP',
      siglaLowerCase: 'ap',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Amazonas',
      sigla: 'AM',
      siglaLowerCase: 'am',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Bahia',
      sigla: 'BA',
      siglaLowerCase: 'ba',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Ceará',
      sigla: 'CE',
      siglaLowerCase: 'ce',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Distrito Federal',
      sigla: 'DF',
      siglaLowerCase: 'df',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Espírito Santo',
      sigla: 'ES',
      siglaLowerCase: 'es',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Goiás',
      sigla: 'GO',
      siglaLowerCase: 'go',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Maranhão',
      sigla: 'MA',
      siglaLowerCase: 'ma',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Mato Grosso',
      sigla: 'MT',
      siglaLowerCase: 'mt',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Mato Grosso do Sul',
      sigla: 'MS',
      siglaLowerCase: 'ms',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Pará',
      sigla: 'PA',
      siglaLowerCase: 'pa',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Paraíba',
      sigla: 'PB',
      siglaLowerCase: 'pb',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Paraná',
      sigla: 'PR',
      siglaLowerCase: 'pr',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Pernambuco',
      sigla: 'PE',
      siglaLowerCase: 'pe',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Piauí',
      sigla: 'PI',
      siglaLowerCase: 'pi',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Rio de Janeiro',
      sigla: 'RJ',
      siglaLowerCase: 'rj',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Rio Grande do Norte',
      sigla: 'RN',
      siglaLowerCase: 'rn',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Rio Grande do Sul',
      sigla: 'RS',
      siglaLowerCase: 'rs',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Rondônia',
      sigla: 'RO',
      siglaLowerCase: 'ro',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Roraima',
      sigla: 'RR',
      siglaLowerCase: 'rr',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Santa Catarina',
      sigla: 'SC',
      siglaLowerCase: 'sc',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'São Paulo',
      sigla: 'SP',
      siglaLowerCase: 'sp',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Sergipe',
      sigla: 'SE',
      siglaLowerCase: 'se',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    },
    {
      nome: 'Tocantins',
      sigla: 'TO',
      siglaLowerCase: 'to',
      fetchPresidentes: true,
      fetchGovernadores: true,
      fetchSenadores: true,
      fetchDeputadosFederais: false,
      fetchDeputadosEstaduais: false
    }
  ]
};
