import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto'
  },

  table: {
    minWidth: 700
  },

  one: {
    backgroundColor: '#27ae60',
    color: '#ecf0f1'
  },

  two: {
    backgroundColor: '#c0392b',
    color: '#ecf0f1'
  },

  many: {
    backgroundColor: '#f1c40f'
  },

  inapto: {
    backgroundColor: 'grey'
  }
});

function AlmgSimpleTable(props) {
  const { classes, columns, values, eleitos } = props;

  const firstEleito = eleitos[0];

  const classEleitos =
    !firstEleito.countEleitos || firstEleito.countEleitos === 0
      ? ''
      : firstEleito.countEleitos === 1
        ? classes.one
        : firstEleito.countEleitos === 2
          ? classes.two
          : classes.many;

  return (
    <Paper>
      <Table>
        <TableHead>
          <TableRow>
            {columns.map(column => (
              <TableCell key={column.id}>{column.description}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {values.map((value, index) => {
            const currentEleito = eleitos[index].eleito;

            /**
             * Definindo as classes
             */
            let className = !!currentEleito ? classEleitos + ' ' : ' ';
            className += !value.apto ? classes.inapto : '';
            className = className.trim();

            return (
              <TableRow key={value.id}>
                {columns.map((column, index) => (
                  <TableCell
                    className={className}
                    key={index + '_' + value.numeroCandidato}
                  >
                    {value[column.columnName]}
                  </TableCell>
                ))}
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
}

AlmgSimpleTable.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlmgSimpleTable);
