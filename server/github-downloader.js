/**
 * Instanciando socket.io
 */
const io = require('socket.io')();

/**
 * Utilizando API para o Git
 */
const Git = require('nodegit');

/**
 * O chalk é utilizado para utilizar
 * cores no console do terminal
 */
const chalk = require('chalk');

/**
 * Apelidando log para facilitar a utilização
 * do chalk
 */
const log = console.log;

/**
 * Pasta de destinos dos repositórios
 * a serem clonados
 */
const destinationBaseFolder = require('os').homedir() + '\\Desktop\\repos\\';

/**
 * Função principal
 */
function initializeService() {
  // prettier-ignore
  //Git.Clone('https://github.com/nodegit/nodegit', destinationBaseFolder)
  //   .then(() => console.log('downloaded'));
}

/**
 * Controle de conexão dos clientes
 */
io.on('connection', client => {
  /**
   * Variável para controlar
   * a quantidade de emissões
   * de dados por parte do
   * servidor
   */
  let runs = 1;

  /**
   * Evento para 'subscribeToSimulacao', que
   * será emitido pelo cliente
   */
  client.on('parseUsers', props => {
    /**
     * Obtendo dados vindos do cliente
     */
    const { usersFile, clientName } = props;

    /**
     * Informamos que houve conexão
     */
    log(
      chalk.yellow("Cliente '" + clientName + "' se inscreveu no service...")
    );

    const users = usersFile.toString('utf-8').split('\r\n');

    /**
     * Realizando a emissão propriamente dita,
     * com base no valor de interval
     */
    client.emit('usersParsed', { users });
  });

  /**
   * Evento para 'subscribeToSimulacao', que
   * será emitido pelo cliente
   */
  client.on('downloadGitRepositoriesFromUsers', props => {
    /**
     * Obtendo dados vindos do cliente
     */
    const { users, clientName } = props;

    /**
     * Informamos que houve conexão
     */
    log(
      chalk.yellow(
        "Cliente '" +
          clientName +
          "' se inscreveu no service de download de repositórios..."
      )
    );

    users.forEach(user => {
      console.log('checking user', user.name);

      Git.Clone(user.repoUrl, destinationBaseFolder + '/' + user.name)
        .then(repo => {
          console.log('downloaded', repo);
        })
        .catch(error => {
          console.log(error);
        });
    });

    /**
     * Realizando a emissão propriamente dita,
     * com base no valor de interval
     */
    //client.emit('downloadsCompleted', simulacaoService.dadosSimulados);
  });
});

/**
 * Definição da porta
 */
const port = 8400;
io.listen(port);

/**
 * Início de tudo
 */
log(
  chalk.green(
    'Servidor de downloads de repositorios do Github ativo na porta',
    port
  )
);

/**
 * Invocando função principal
 */
initializeService();
