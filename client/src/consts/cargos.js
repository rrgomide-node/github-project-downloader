export const CARGO_PRESIDENTE = 0;
export const CARGO_GOVERNADOR = 1;
export const CARGO_SENADOR = 2;
export const CARGO_DEPUTADO_FEDERAL = 3;
export const CARGO_DEPUTADO_ESTADUAL = 4;
export const GRAFICOS = 5;

export const ALL_CARGOS = [
  {
    id: CARGO_PRESIDENTE,
    value: 'Presidente'
  },
  {
    id: CARGO_GOVERNADOR,
    value: 'Governador'
  },
  {
    id: CARGO_SENADOR,
    value: 'Senador'
  },
  {
    id: CARGO_DEPUTADO_FEDERAL,
    value: 'Dep. Federal'
  },
  {
    id: CARGO_DEPUTADO_ESTADUAL,
    value: 'Dep. Estadual'
  }
];

export const cargosPrimeiroTurno = [
  { id: CARGO_PRESIDENTE, cargo: 'Presidente', titulo: 'Pres.' },
  { id: CARGO_GOVERNADOR, cargo: 'Governador', titulo: 'Gov.' },
  { id: CARGO_SENADOR, cargo: 'Senador', titulo: 'Sen.' },
  {
    id: CARGO_DEPUTADO_FEDERAL,
    cargo: 'Deputado Federal',
    titulo: 'Fed. (MG)'
  },
  {
    id: CARGO_DEPUTADO_ESTADUAL,
    cargo: 'Deputado Estadual',
    titulo: 'Est. (MG)'
  }
];

export const cargosSegundoTurno = [
  { id: CARGO_PRESIDENTE, cargo: 'Presidente', titulo: 'Presidente' },
  { id: CARGO_GOVERNADOR, cargo: 'Governador', titulo: 'Governador' }
];
