const numeral = require('numeral');

/**
 * Fonte: https://github.com/adamwdraper/Numeral-js/blob/master/locales/pt-br.js
 */
numeral.register('locale', 'pt-br', {
  delimiters: {
    thousands: '.',
    decimal: ','
  },
  abbreviations: {
    thousand: 'mil',
    million: 'milhões',
    billion: 'b',
    trillion: 't'
  },
  ordinal: function(number) {
    return 'º';
  },
  currency: {
    symbol: 'R$'
  }
});

numeral.locale('pt-br');

class NumeralService {
  /**
   * Formata valores no padrão pt-br
   * @param {number} valueToFormat
   */
  static formatValue(valueToFormat) {
    const numeralValue = numeral(valueToFormat);
    return numeralValue.format();
  }

  static formatPercentual(valueToFormat) {
    const percentualNumeral = numeral(valueToFormat);
    const resto = valueToFormat % 1;

    if (resto === 0) {
      return percentualNumeral.format('0') + '%';
    } else {
      return percentualNumeral.format('0.0') + '%';
    }
  }
}

module.exports = NumeralService;
