/**
 * Inspired by: https://reactjs.org/docs/uncontrolled-components.html#the-file-input-tag
 * Inspired by: https://material-ui.com/demos/buttons/
 */
import React, { Component } from 'react';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit
  },

  input: {
    display: 'none'
  }
});

class GomideFileInput extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.fileInput = React.createRef();
  }

  handleSubmit(event) {
    const { onHandleUpload } = this.props;
    onHandleUpload(this.fileInput.current.files[0]);
  }

  render() {
    const { classes, className } = this.props;

    return (
      <div className={className}>
        <input
          accept=".txt"
          className={classes.input}
          id="raised-button-file"
          type="file"
          ref={this.fileInput}
          onChange={this.handleSubmit}
        />
        <label htmlFor="raised-button-file">
          {!!this.props.children ? (
            this.props.children
          ) : (
            <Button
              variant="contained"
              color="primary"
              component="span"
              className={classes.button}
            >
              Carregar arquivo de usuários do Github
            </Button>
          )}
        </label>
      </div>
    );
  }
}

GomideFileInput.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(GomideFileInput);
