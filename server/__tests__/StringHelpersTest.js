const StringHelpers = require('../src/helpers/StringHelpers');

test('LeftPad with negative digits', () => {
  expect(StringHelpers.leftPad('0', -1, '0')).toBe('0');
  expect(StringHelpers.leftPad('1', -1, '_')).toBe('1');
});

test('LeftPad with zero digits', () => {
  expect(StringHelpers.leftPad('0', 0, '0')).toBe('0');
  expect(StringHelpers.leftPad('1', 0, '_')).toBe('1');
});

test('LeftPad with one digit', () => {
  expect(StringHelpers.leftPad('0', 1, '0')).toBe('0');
  expect(StringHelpers.leftPad('1', 1, '_')).toBe('1');
});

test('LeftPad with two digits', () => {
  expect(StringHelpers.leftPad('1', 2, '0')).toBe('01');
  expect(StringHelpers.leftPad('2')).toBe('02');
});

test('LeftPad with three digits', () => {
  expect(StringHelpers.leftPad('3', 3, '0')).toBe('003');
  expect(StringHelpers.leftPad('4', 3, '_')).toBe('__4');
});

test('LeftPad with ten digits', () => {
  expect(StringHelpers.leftPad('5', 10, '0')).toBe('0000000005');
  expect(StringHelpers.leftPad('6', 10, '_')).toBe('_________6');
});

test('LeftPad with numbers', () => {
  expect(() => StringHelpers.leftPad(5, 10, '0')).toThrow();
  expect(() => StringHelpers.leftPad(6, 10, '_')).toThrow();
});
