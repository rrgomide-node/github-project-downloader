import React from 'react';

import './gomideTitle.css';

const GomideTitle = props => {
  return <div className="gomide-title">{props.children}</div>;
};

export default GomideTitle;
